/*
 * event.cpp
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#include "event.h"
#include "kernev.h"
#include "ivtentry.h"

Event::Event(IVTNo ivtNo) {
    asm cli;
    myImpl = new KernelEv;
    IVTEntry::connect(ivtNo, myImpl);
    asm sti;
}

Event::~Event() {
    asm cli;
    delete myImpl;
    asm sti;
}

void Event::wait() {
    myImpl->wait();
}

void Event::signal() {
}
