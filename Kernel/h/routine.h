/*
 * routine.h
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#ifndef ROUTINE_H_
#define ROUTINE_H_

#include "pcb.h"
#include "schedule.h"

void initNewRoutine();
void restoreOldRoutine();

void interrupt systemTimer();

#endif /* ROUTINE_H_ */
