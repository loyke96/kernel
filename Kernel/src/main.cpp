/*
 * main.cpp
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#include "routine.h"
#include "idleThr.h"
#include "ivtentry.h"

extern int userMain(int, char*[]);

PCB* PCB::runningPCB;
IntReason PCB::iReason = TICK;
Buffer PCB::sleeped;
KernelEv* IVTEntry::ivtEv;
int IVTEntry::oldRoutNeed = 0;

int main(int argc, char* argv[]) {
    PCB* mainPCB = new PCB(0, 0, 2);
    IdleThread* idlT = new IdleThread;
    PCB::runningPCB = mainPCB;
    idlT->start();
    initNewRoutine();
    int uM = userMain(argc, argv);
    restoreOldRoutine();
    delete mainPCB;
    asm int 61h;
    return uM;
}
