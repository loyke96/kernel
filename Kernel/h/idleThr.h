/*
 * idleThr.h
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#include <IOSTREAM.H>

class IdleThread : public Thread {
public:

    IdleThread() : Thread(64, 1) {
    }

    virtual ~IdleThread() {
    }

protected:

    virtual void run() {
        while (1) {
            for (int i = 0; i < 10000; i++) {
                for (int j = 0; j < 10000; j++);
            }
        }
    }
};

