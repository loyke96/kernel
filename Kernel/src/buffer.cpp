/*
 * buffer.cpp
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#include "buffer.h"
#include "pcb.h"
#include "schedule.h"

void Buffer::put(PCB* pcb) {
    while (buff[last] != 0) {
        last = (last + 1) % 25;
    }
    buff[last++] = pcb;
}

PCB* Buffer::get() {
    int i = 0;
    while (buff[i] == 0) {
        ++i;
    }
    PCB* ret = buff[i];
    buff[i] = 0;
    return ret;
}

void Buffer::wake() {
    for (int i = 0; i < 25; ++i) {
        if (buff[i] && --(buff[i]->sleepTime) == 0) {
            Scheduler::put(buff[i]);
            buff[i] = 0;
        }
    }
}
