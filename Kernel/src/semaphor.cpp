/*
 * semaphor.cpp
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#include "semaphor.h"
#include "kersem.h"

Semaphore::Semaphore(int init) {
    asm cli;
    myImpl = new KernelSem(init);
    asm sti;
}

Semaphore::~Semaphore() {
    asm cli;
    delete myImpl;
    asm sti;
}

void Semaphore::wait() {
    myImpl->wait();
}

void Semaphore::signal() {
    myImpl->signal();
}

int Semaphore::val() const {
    return myImpl->val();
}
