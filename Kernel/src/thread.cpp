/*
 * thread.cpp
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#include "thread.h"
#include "pcb.h"
#include "schedule.h"

void Thread::start() {
    Scheduler::put(myPCB);
}

void Thread::waitToComplete() {
    myPCB->waitToComplete();
}

Thread::~Thread() {
    waitToComplete();
    asm cli;
    delete myPCB;
    asm sti;
}

void Thread::sleep(Time timeToSleep) {
    PCB::sleep(timeToSleep);
}

Thread::Thread(StackSize stackSize, Time timeSlice) {
    asm cli;
    myPCB = new PCB(this, stackSize, timeSlice);
    asm sti;
}

void dispatch() {
    Scheduler::put(PCB::runningPCB);
    PCB::iReason = DISPATCH;
    systemTimer();
}
