/*
 * buffer.h
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#ifndef BUFFER_H_
#define BUFFER_H_

class PCB;

class Buffer {
    PCB* buff[25];
    int last;

public:

    Buffer() : last(0) {
        for (int i = 0; i < 25; ++i) {
            buff[i] = 0;
        }
    }
    void put(PCB* pcb);
    PCB* get();
    void wake();
};

#endif /* BUFFER_H_ */
