/*
 * KernelEv.h
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#ifndef KERNEV_H_
#define KERNEV_H_

#include "event.h"

class PCB;

class KernelEv {
    PCB* evPCB;

public:
    KernelEv();
    void wait();
    void signal();
};

#endif /* KERNEV_H_ */
