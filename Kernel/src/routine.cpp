/*
 * routine.cpp
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#include "routine.h"
#include <IOSTREAM.H>

volatile unsigned oldSeg, oldOff;

volatile unsigned tempSS, tempSP;

volatile Time timeSlice = 2;

void interrupt systemTimer() {
    switch (PCB::iReason) {
        case TICK:
            PCB::sleeped.wake();
            asm int 60h;
            if (--timeSlice != 0) break;
            Scheduler::put(PCB::runningPCB);
        case DISPATCH: case SLEEP: case BLOCK: case FINNISH:
            asm {
                    mov tempSS, ss
                    mov tempSP, sp
                }
            PCB::runningPCB->stackSeg = tempSS;
            PCB::runningPCB->stackOff = tempSP;
            PCB::runningPCB = Scheduler::get();
            tempSP = PCB::runningPCB->stackOff;
            tempSS = PCB::runningPCB->stackSeg;
            timeSlice = PCB::runningPCB->timeSlice;
            asm {
                    mov ss, tempSS
                    mov sp, tempSP
                }
            PCB::iReason = TICK;
    }
}

void initNewRoutine() {
    asm {
            cli
            push es
            push ax
            push di
            mov ax, 0
            mov es, ax

            mov di, 22h
            mov ax, word ptr es : di
            mov word ptr oldSeg, ax
            mov word ptr es : di, seg systemTimer

            mov di, 20h
            mov ax, word ptr es : di
            mov word ptr oldOff, ax
            mov word ptr es : di, offset systemTimer

            mov di, 180h
            mov ax, word ptr oldOff
            mov word ptr es : di, ax
            mov di, 182h
            mov ax, word ptr oldSeg
            mov word ptr es : di, ax

            pop di
            pop ax
            pop es
            sti
        }
}

void restoreOldRoutine() {
    asm {
            cli
            push es
            push ax
            push di

            mov ax, 0
            mov es, ax

            mov di, 22h
            mov ax, word ptr oldSeg
            mov word ptr es : di, ax

            mov di, 20h
            mov ax, word ptr oldOff
            mov word ptr es : di, ax

            pop di
            pop ax
            pop es
            sti
        }
}
