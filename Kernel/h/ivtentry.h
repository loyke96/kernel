/*
 * ivtentry.h
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#ifndef IVTENTRY_H_
#define IVTENTRY_H_

#include "event.h"

class IVTEntry {
    static KernelEv* ivtEv;

    static int oldRoutNeed;

    unsigned oldRoutS, oldRoutO;

public:
    IVTEntry(IVTNo ivt, int oldR, void (interrupt *func)());
    ~IVTEntry();
    static void connect(IVTNo ivt, KernelEv* kEv);
    static void call(IVTNo ivt);
};

#define PREPAREENTRY(N, B)          \
        void interrupt interr##N() {\
                IVTEntry::call(N);  \
        }                           \
        IVTEntry entryIVT##N(N, B, interr##N);

#endif /* IVTENTRY_H_ */
