/*
 * kernev.cpp
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#include "kernev.h"
#include "pcb.h"
#include "schedule.h"

KernelEv::KernelEv() {
    evPCB = PCB::runningPCB;
}

void KernelEv::wait() {
    if (evPCB == PCB::runningPCB) {
        PCB::iReason = BLOCK;
        systemTimer();
    }
}

void KernelEv::signal() {
    Scheduler::put(evPCB);
}
