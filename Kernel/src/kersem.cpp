/*
 * kersem.cpp
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#include "kersem.h"
#include "pcb.h"
#include "schedule.h"
#include <IOSTREAM.H>

KernelSem::KernelSem(int init) {
    value = init;
}

void KernelSem::wait() {
    if (--value < 0) {
        stop.put(PCB::runningPCB);
        PCB::iReason = BLOCK;
        systemTimer();
    }
}

void KernelSem::signal() {
    if (++value <= 0)
        Scheduler::put(stop.get());
}

int KernelSem::val() {
    return value;
}
