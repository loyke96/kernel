/*
 * pcb.cpp
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#include <dos.h>
#include <IOSTREAM.H>

#include "pcb.h"
#include "schedule.h"
#include "routine.h"

PCB::PCB(Thread* thread, StackSize stackSize, Time timeSlice) {
    myThread = thread;
    stackSize /= 2;
    stack = new unsigned[stackSize];
    this->timeSlice = timeSlice;
    stack[stackSize - 1] = 0x200;
    stack[stackSize - 2] = FP_SEG(wrapper);
    stack[stackSize - 3] = FP_OFF(wrapper);
    stackSeg = FP_SEG(stack + stackSize - 12);
    stackOff = FP_OFF(stack + stackSize - 12);
    isFinnished = 0;
    blockedPCB = 0;
    sleepTime = 0;
}

PCB::~PCB() {
    delete[] stack;
}

void PCB::waitToComplete() {
    if (isFinnished == 0) {
        blockedPCB = runningPCB;
        iReason = BLOCK;
        systemTimer();
    }
}

void PCB::sleep(Time timeToSleep) {
    runningPCB->sleepTime = timeToSleep;
    sleeped.put(runningPCB);
    iReason = SLEEP;
    systemTimer();
}

void PCB::wrapper() {
    runningPCB->myThread->run();
    runningPCB->isFinnished = 1;
    if (runningPCB->blockedPCB) {
        Scheduler::put(runningPCB->blockedPCB);
    }
    iReason = FINNISH;
    systemTimer();
}
