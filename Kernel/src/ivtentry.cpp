/*
 * ivtentry.cpp
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#include "ivtentry.h"
#include "kernev.h"
#include <DOS.H>

volatile unsigned segAddr, offAddr;
volatile unsigned funcS, funcO;
volatile unsigned oS, oO;

IVTEntry::IVTEntry(IVTNo ivt, int oldR, void (interrupt *func)()) {
    offAddr = ivt * 4;
    segAddr = offAddr + 2;
    oldRoutNeed = oldR;
    funcS = FP_SEG(func);
    funcO = FP_OFF(func);
    asm {
            cli
            push es
            push ax
            push di
            mov ax, 0
            mov es, ax

            mov di, word ptr segAddr
            mov ax, word ptr es : di
            mov word ptr oS, ax
            mov ax, word ptr funcS
            mov word ptr es : di, ax

            mov di, word ptr offAddr
            mov ax, word ptr es : di
            mov word ptr oO, ax
            mov ax, word ptr funcO
            mov word ptr es : di, ax

            mov di, 184h
            mov ax, word ptr oO
            mov word ptr es : di, ax
            mov di, 186h
            mov ax, word ptr oS
            mov word ptr es : di, ax

            pop di
            pop ax
            pop es
            sti
        }
}

void IVTEntry::connect(IVTNo ivt, KernelEv* kEv) {
    ivtEv = kEv;
}

void IVTEntry::call(IVTNo ivt) {
    ivtEv->signal();
    if (oldRoutNeed) asm int 61h;
}

IVTEntry::~IVTEntry() {
    asm {
            cli
            push es
            push ax
            push di

            mov ax, 0
            mov es, ax

            mov di, segAddr
            mov ax, word ptr oS
            mov word ptr es : di, ax

            mov di, offAddr
            mov ax, word ptr oO
            mov word ptr es : di, ax

            pop di
            pop ax
            pop es
            sti
        }
}
