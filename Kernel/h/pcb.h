/*
 * pcb.h
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#ifndef PCB_H_
#define PCB_H_

#include "thread.h"
#include "buffer.h"

enum IntReason {
    TICK, DISPATCH, SLEEP, BLOCK, FINNISH
};

class PCB {
    PCB(Thread* thread, StackSize stackSize, Time timeSlice);
    ~PCB();
    void waitToComplete();

    static void sleep(Time timeToSleep);
    static void wrapper();

    static IntReason iReason;
    static PCB* runningPCB;
    static Buffer sleeped;

    Thread* myThread;
    int isFinnished;
    unsigned* stack;
    unsigned stackSeg, stackOff;
    Time timeSlice;
    Time sleepTime;
    PCB* blockedPCB;

    friend class Thread;
    friend class Buffer;
    friend class KernelSem;
    friend class KernelEv;
    friend void dispatch();
    friend void interrupt systemTimer();
    friend int main(int, char*[]);
};

#endif /* PCB_H_ */
