/*
 * KernelSem.h
 *
 *  Created on: Aug 10, 2017
 *      Author: OS1
 */

#ifndef KERSEM_H_
#define KERSEM_H_

#include "buffer.h"

class KernelSem {
    int value;
    Buffer stop;

public:
    KernelSem(int init);
    void wait();
    void signal();
    int val();
};

#endif /* KERSEM_H_ */
